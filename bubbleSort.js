var bubbleSort = function(arr) {
  var finishedArr = [];
  if (arr.length === 0) {
    return finishedArr;
  }
  arr.forEach(function(ele) {
    insertInto(finishedArr, ele);  
  })
  return finishedArr;
};

var insertInto = function(arr, element) {
  for (i = 0; i <= arr.length; i++) {
    if (arr[i] === undefined) {
      arr.push(element);
      break;
    } else if (element <= arr[i]) {
      arr.splice(i, 0, element);
      break;
    }
  }
}

module.exports = bubbleSort;

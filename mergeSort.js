var mergeSort = function(arr) {
  var opArr = arr.map(ele => ele = [ele]);
  while (opArr.length > 1) {
    combine(opArr);
  }
  return [].concat.apply([], opArr);
}

var combine = function(arr) {
  for (i = 0; i < arr.length; i += 2) {
    var arr1 = arr[i];
    var arr2 = arr[i + 1];
    if (arr2 !== undefined) {
      var start = 0;
      arr2.forEach(function(ele) {
        compare(start, ele, arr1);
      });
      arr.splice((i + 1), 1);
    }
  }
}

var compare = function(ind, element, arr) {
  for (ind; ind <= arr.length; ind++) {
    if (arr[ind] === undefined) {
      arr.push(element);
      break;
    } else if (element <= arr[ind]) {
      arr.splice(ind, 0, element);
      break;
    }
  }
}

module.exports = mergeSort;

const pry = require('pryjs');

var quickSort = function(arr) {
  if (arr.length > 1) {
    var pivot = arr[parseInt(arr.length / 2)];
    var center = [];
    var right = [];
    var left = [];
    arr.forEach(function(element) {
      if (element > pivot) {
        right.unshift(element);
      } else if (element == pivot) {
        center.unshift(element);
      } else {
        left.unshift(element);
      }
    });
    right = quickSort(right);
    left = quickSort(left);
    return left.concat(center).concat(right);
  } else {
    return arr;
  }
}

module.exports = quickSort;

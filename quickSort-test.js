const quickSort = require('./quickSort');
const pry = require('pryjs');
const assert = require('chai').assert;

describe('Quick Sort Tests', function() {
  it('it can sort', function() {
    var arr = [4, 3, 2, 1];
    var sortedArr = quickSort(arr);
    var expectedArr = [1, 2, 3, 4];
    assert.deepEqual(expectedArr, sortedArr);
  });

  it('can sort something else', function() {
    var arr = [3, 0, 9, 2, 5, 6, 1, 4, 4];
    var sortedArr = quickSort(arr);
    var expectedArr = [0, 1, 2, 3, 4, 4, 5, 6, 9];
    assert.deepEqual(expectedArr, sortedArr);
  });

  it('will return empty array if called on empty array', function() {
    var arr = [];
    var sortedArr = quickSort(arr);
    assert.deepEqual(arr, sortedArr);
  });
});
